﻿// ***********************************************************************
// Assembly         : FolderCustomizer
// Author           : Cortese Mauro Luigi
// Created          : 05-11-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 05-11-2015
// ***********************************************************************
// <copyright file="MyFile.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderCustomizer
{
    /// <summary>
    /// Class MyFile.
    /// </summary>
    public class MyFile
    {
        #region Costanti
        #endregion

        #region Enumerazioni
        /// <summary>
        /// Enum ModeListComposer
        /// </summary>
        public enum ModeListComposer
        {
            /// <summary>
            /// The full path
            /// </summary>
            FullPath,

            /// <summary>
            /// The only file name
            /// </summary>
            OnlyFileName
        }
        #endregion

        #region Campi
        #endregion

        #region Costruttori
        /// <summary>
        /// Initializes a new instance of the <see cref="MyFile" /> class.
        /// </summary>
        /// <param name="path">The path.</param>
        public MyFile(string path)
        {
            try
            {
                this.path = path;
                this.name = System.IO.Path.GetFileName(path);
                this.extension = System.IO.Path.GetExtension(path);
                this.folder = System.IO.Path.GetDirectoryName(path);
                this.hasException = false;
            }
            catch (Exception ex)
            {
                this.hasException = true;
                this.exception = ex;
                this.folder = path.Substring(0,path.Length-this.name.Length);
            }

        }
        #endregion

        #region Proprietà
        /// <summary>
        /// The path
        /// </summary>
        private string path;
        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>The path.</value>
        public string Path { get { return this.path; } set { this.path = value; } }

        /// <summary>
        /// The name
        /// </summary>
        private string name;
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get { return this.name; } set { this.name = value; } }

        /// <summary>
        /// The extension
        /// </summary>
        private string extension;
        /// <summary>
        /// Gets or sets the extension.
        /// </summary>
        /// <value>The extension.</value>
        public string Extension { get { return this.extension; } set { this.extension = value; } }

        /// <summary>
        /// The folder
        /// </summary>
        private string folder;
        /// <summary>
        /// Gets or sets the folder.
        /// </summary>
        /// <value>The folder.</value>
        public string Folder { get { return this.folder; } set { this.folder = value; } }

        /// <summary>
        /// The exception
        /// </summary>
        private Exception exception;
        /// <summary>
        /// Gets or sets the exception.
        /// </summary>
        /// <value>The exception.</value>
        public Exception Exception { get { return this.exception; } }

        /// <summary>
        /// The has exception
        /// </summary>
        private bool hasException;
        /// <summary>
        /// Gets a value indicating whether this instance has exception.
        /// </summary>
        /// <value><c>true</c> if this instance has exception; otherwise, <c>false</c>.</value>
        public bool HasException { get { return this.hasException; } }
        #endregion

        #region Metodi pubblici
        #endregion

        #region Handlers eventi
        #endregion

        #region Metodi privati
        #endregion

        #region Definizione eventi
        #endregion

        #region Tipi nidificati
        #endregion




    }
}
