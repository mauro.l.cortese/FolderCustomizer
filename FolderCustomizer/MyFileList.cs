﻿// ***********************************************************************
// Assembly         : FolderCustomizer
// Author           : Cortese Mauro Luigi
// Created          : 05-11-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 05-11-2015
// ***********************************************************************
// <copyright file="MyFile.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderCustomizer
{
    /// <summary>
    /// Class ListMyFile.
    /// </summary>
    public class ListMyFile : List<MyFile>
    {

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            switch (this.modeList)
            {
                case MyFile.ModeListComposer.FullPath:
                    foreach (MyFile myFile in this)
                    {
                        if (this.WithExtension)
                            sb.AppendLine(myFile.Path);
                        else
                        {
                            string ext = Path.GetExtension(myFile.Path);
                            sb.AppendLine(myFile.Path.Substring(0, myFile.Path.Length - ext.Length));
                        }
                    }
                    break;


                case MyFile.ModeListComposer.OnlyFileName:

                    List<string> filesName = new List<string>();
                    foreach (MyFile myFile in this)
                        if (!filesName.Contains(myFile.Name))
                            filesName.Add(myFile.Name);

                    filesName.Sort();
                    foreach (string file in filesName)
                    {
                        if (this.WithExtension)
                            sb.AppendLine(file);
                        else
                        {
                            string ext = Path.GetExtension(file);
                            sb.AppendLine(file.Substring(0, file.Length - ext.Length));
                        }
                    }

                    break;
            }


            return sb.ToString();
        }

        /// <summary>
        /// The mode list
        /// </summary>
        private MyFile.ModeListComposer modeList = MyFile.ModeListComposer.FullPath;
        /// <summary>
        /// Gets or sets the mode list.
        /// </summary>
        /// <value>The mode list.</value>
        public MyFile.ModeListComposer ModeList { get { return this.modeList; } set { this.modeList = value; } }

        /// <summary>
        /// Gets or sets a value indicating whether [with extension].
        /// </summary>
        /// <value><c>true</c> if [with extension]; otherwise, <c>false</c>.</value>
        public bool WithExtension { get; set; }
    }
}
