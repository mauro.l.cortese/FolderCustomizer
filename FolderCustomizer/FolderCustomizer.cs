﻿// ***********************************************************************
// Assembly         : FolderCustomizer
// Author           : Cortese Mauro Luigi
// Created          : 05-08-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 05-11-2015
// ***********************************************************************
// <copyright file="FolderCustomizer.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace FolderCustomizer
{
	/// <summary>
	/// Class FolderCustomizer.
	/// </summary>
	public class FolderCustomizer
	{
		/// <summary>
		/// The ini text
		/// </summary>
		StringBuilder iniText = new StringBuilder(); // http://hwiegman.home.xs4all.nl/desktopini.html

		/// <summary>
		/// The file ini
		/// </summary>
		string fileIni = null;
		/// <summary>
		/// The comment
		/// </summary>
		public string Comment = null;
		/// <summary>
		/// The path folder image
		/// </summary>
		public string PathFolderImage = null;
		/// <summary>
		/// The path
		/// </summary>
		public string Path;

		/// <summary>
		/// Initializes a new instance of the <see cref="FolderCustomizer"/> class.
		/// </summary>
		/// <param name="pathFolder">The path folder.</param>
		public FolderCustomizer(string pathFolder)
		{
			this.dirCheck(pathFolder);
			this.LoadIni();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FolderCustomizer"/> class.
		/// </summary>
		public FolderCustomizer()
		{
			// TODO: Complete member initialization
		}

		/// <summary>
		/// Loads the ini.
		/// </summary>
		internal void LoadIni()
		{
			if (File.Exists(this.fileIni))
				this.iniText = new StringBuilder(File.ReadAllText(fileIni));

			this.Comment = this.getVal("InfoTip");
			this.PathFolderImage = this.getVal("IconResource");
			if (!string.IsNullOrEmpty(this.PathFolderImage))
			{
				int pos = this.PathFolderImage.IndexOf(",");
				if (pos != -1)
					this.PathFolderImage = this.PathFolderImage.Substring(0, pos);
			}

		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>System.String.</returns>
		private string getVal(string key)
		{
			if (this.iniText.Length == 0)
				return string.Empty;
			int pos = 0;
			int posl = 0;
			string txt = this.iniText.ToString();
			pos = txt.IndexOf(key + "=");
			if (pos != -1)
				posl = txt.IndexOf(Environment.NewLine, pos);
			if (posl == -1)
				posl = txt.IndexOf('\n', pos);
			if (posl == -1)
				posl = txt.Length;

				string retVal = null;
			if (pos != -1 && posl != -1)
				retVal = txt.Substring(pos, posl - pos);

			if (pos != -1)
			{
				pos = retVal.IndexOf("=");
				retVal = retVal.Substring(pos + 1);
			}
			return retVal;
		}

		/// <summary>
		/// Writes the ini.
		/// </summary>
		private void writeIni()
		{
			if (fileIni == null)
				return;

			if (this.iniText.Length == 0)
			{
				this.iniText.AppendLine("[.ShellClassInfo]");
				this.iniText.AppendLine("InfoTip=" + this.Comment);
				this.iniText.AppendLine("IconResource=" + this.PathFolderImage);
			}
			else
			{
				this.setVal("InfoTip", this.Comment);
				this.setVal("IconResource", this.PathFolderImage + ",0");
			}

			if (File.Exists(fileIni))
				File.SetAttributes(fileIni, FileAttributes.Normal);

			this.iniText.Replace(Environment.NewLine, "\n");
			File.WriteAllText(fileIni, this.iniText.ToString());
			File.SetAttributes(fileIni, FileAttributes.System | FileAttributes.Hidden);


			//[.ShellClassInfo]
			//InfoTip=Prove
			//IconResource=C:\FsIcons\Doc\Cad.ico,0
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		private void setVal(string key, string value)
		{
			int pos = 0;
			int posl = 0;
			string txt = this.iniText.ToString();
			pos = txt.IndexOf(key + "=");
			if (pos != -1)
				posl = txt.IndexOf(Environment.NewLine, pos);
			if (posl == -1)
				posl = txt.IndexOf('\n', pos);
			if (posl == -1)
				posl = txt.Length;

			if (pos != -1)
			{
				this.iniText.Remove(pos, posl - pos);
				this.iniText.Insert(pos, key + "=" + value);
			}
			if (pos == -1 && !string.IsNullOrEmpty(value))
				this.iniText.AppendLine(key + "=" + value);
		}

		/// <summary>
		/// Gets the folder image.
		/// </summary>
		/// <value>The folder image.</value>
		public Image FolderImage
		{
			get
			{
				if (File.Exists(this.PathFolderImage))
					return Image.FromFile(this.PathFolderImage);
				else
					return null;
			}
		}

		/// <summary>
		/// Resets the customization.
		/// </summary>
		internal void ResetCustomization()
		{
			this.dirCheck(this.Path);
			if (File.Exists(this.fileIni))
			{
				FileInfo fileInfo = new FileInfo(this.fileIni);
				fileInfo.Attributes = FileAttributes.Normal;
				File.Delete(this.fileIni);
			}
			if (Directory.Exists(this.Path))
			{
				DirectoryInfo directoryInfo = new DirectoryInfo(this.Path);
				directoryInfo.Attributes = FileAttributes.Normal;
			}
		}

		/// <summary>
		/// Applies the customization.
		/// </summary>
		internal void ApplyCustomization()
		{
			if (this.Path == null)
				return;

			this.dirCheck(this.Path);
			this.writeIni();
			DirectoryInfo di = new DirectoryInfo(this.Path);
			di.Attributes = FileAttributes.System;
			if (this.ReadOnly)
				di.Attributes = di.Attributes | FileAttributes.ReadOnly;
			if (this.Hidden)
				di.Attributes = di.Attributes | FileAttributes.Hidden;
			else
				di.Attributes = di.Attributes | FileAttributes.Normal;


		}

		/// <summary>
		/// Dir the check.
		/// </summary>
		/// <param name="pathFolder">The path folder.</param>
		private void dirCheck(string pathFolder)
		{
			if (pathFolder == null)
				return;
			if (this.Path != pathFolder)
				this.Path = pathFolder;
			if (!Directory.Exists(this.Path))
				Directory.CreateDirectory(this.Path);
			this.fileIni = string.Concat(this.Path + "\\Desktop.ini");
		}

		/// <summary>
		/// Gets or sets a value indicating whether the current folder should be set to hidden.
		/// </summary>
		/// <value><c>true</c> if hidden; otherwise, <c>false</c>.</value>
		public bool Hidden { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the current folder should be set to read only.
		/// </summary>
		/// <value><c>true</c> if [read only]; otherwise, <c>false</c>.</value>
		public bool ReadOnly { get; set; }

		/// <summary>
		/// Gets or sets the ini text.
		/// </summary>
		/// <value>The ini text.</value>
		public string IniText { get { return this.iniText.ToString(); } set { this.iniText = new StringBuilder(value); } }
	}
}
