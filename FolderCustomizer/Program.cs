﻿// ***********************************************************************
// Assembly         : FolderCustomizer
// Author           : Cortese Mauro Luigi
// Created          : 05-08-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 05-11-2015
// ***********************************************************************
// <copyright file="Program.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FolderCustomizer
{
    /// <summary>
    /// Class Program.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            // Environment.SetEnvironmentVariable("FsIcons", "C:\\Prova", EnvironmentVariableTarget.User);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }
    }
}
