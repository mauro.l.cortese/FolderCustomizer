﻿// ***********************************************************************
// Assembly         : FolderCustomizer
// Author           : Cortese Mauro Luigi
// Created          : 05-08-2015
//
// Last Modified By : Cortese Mauro Luigi
// Last Modified On : 05-08-2015
// ***********************************************************************
// <copyright file="Form1.cs" company="Microsoft">
//     Copyright ©  2013
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FolderCustomizer.Properties;
using System.Linq;


namespace FolderCustomizer
{
    /// <summary>
    /// Class Form1.
    /// </summary>
    public partial class FormMain : Form
    {

        /// <summary>
        /// The pictureBox
        /// </summary>
        private PictureBox pictureBox;

        /// <summary>
        /// The folder customizer
        /// </summary>
        private FolderCustomizer folderCustomizer = new FolderCustomizer();

        /// <summary>
        /// The file list
        /// </summary>
        private ListMyFile fileList = new ListMyFile();


        /// <summary>
        /// Initializes a new instance of the <see cref="FormMain"/> class.
        /// </summary>
        public FormMain()
        {
            InitializeComponent();
        }



        /// <summary>
        /// Handles the Load event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void formMain_Load(object sender, EventArgs e)
        {
            this.Text = string.Concat(Application.ProductName, " - ", Application.ProductVersion);
            this.loadTree(Settings.Default.RootFolderIcons);
            string[] args = System.Environment.GetCommandLineArgs();
            if (args != null && args.Length == 2)
            {
                this.folderCustomizer = new FolderCustomizer(args[1]);
                this.loadFolderData();
            }
            this.tabbedControlGroupIcons.SelectedTabPage = this.layoutControlGroupIcons;
        }

        /// <summary>
        /// Loads the tree.
        /// </summary>
        /// <param name="rootFolderIcons">The root folder icons.</param>
        private void loadTree(string rootFolderIcons)
        {
            this.treeView1.Nodes.Clear();
            TreeNode node = this.treeView1.Nodes.Add(rootFolderIcons);
            node.Tag = rootFolderIcons;
            foreach (string folder in Directory.GetDirectories(rootFolderIcons, "*", SearchOption.TopDirectoryOnly))
            {
                TreeNode nodeChild = node.Nodes.Add(Path.GetFileName(folder));
                nodeChild.Tag = folder;
            }

            this.treeView1.ExpandAll();

        }

        /// <summary>
        /// Handles the NodeMouseClick event of the treeView1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TreeNodeMouseClickEventArgs"/> instance containing the event data.</param>
        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            string folder = (string)e.Node.Tag;
            this.loadIcons(folder);
        }

        /// <summary>
        /// Loads the icons.
        /// </summary>
        /// <param name="folder">The folder.</param>
        private void loadIcons(string folder)
        {
            string[] icons = Directory.GetFiles(folder, "*.ico", SearchOption.TopDirectoryOnly);
            this.flowLayoutPanelIcons.Controls.Clear();
            foreach (string iconFile in icons)
            {
                PictureBox picture = new PictureBox();
                picture.Image = System.Drawing.Image.FromFile(iconFile);
                picture.Margin = new System.Windows.Forms.Padding(1);
                picture.Size = new System.Drawing.Size(38, 38);
                picture.Padding = new System.Windows.Forms.Padding(3);
                picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                picture.Tag = iconFile;
                picture.MouseClick += new MouseEventHandler(picture_MouseClick);
                picture.MouseEnter += new EventHandler(picture_MouseEnter);

                this.flowLayoutPanelIcons.Controls.Add(picture);
            }
        }

        /// <summary>
        /// Handles the MouseEnter event of the picture control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        void picture_MouseEnter(object sender, EventArgs e)
        {
            if (pictureBox != null)
                pictureBox.BorderStyle = BorderStyle.None;

            ((PictureBox)sender).BorderStyle = BorderStyle.FixedSingle;
            pictureBox = ((PictureBox)sender);
        }

        /// <summary>
        /// Handles the MouseClick event of the picture control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
        void picture_MouseClick(object sender, MouseEventArgs e)
        {
            string pathImage = ((PictureBox)sender).Tag.ToString();
            this.folderCustomizer.PathFolderImage = pathImage;
            this.setImage();
        }

        /// <summary>
        /// Handles the DragEnter event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragEventArgs"/> instance containing the event data.</param>
        private void formMain_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
        }

        /// <summary>
        /// Handles the DragDrop event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragEventArgs"/> instance containing the event data.</param>
        private void formMain_DragDrop(object sender, DragEventArgs e)
        {
            string[] folder = (string[])e.Data.GetData(DataFormats.FileDrop);
            this.folderCustomizer = new FolderCustomizer(folder[0]);
            this.loadFolderData();
        }

        /// <summary>
        /// Loads the folder data.
        /// </summary>
        private void loadFolderData()
        {
            this.textBoxFolderPath.Text = this.folderCustomizer.Path;
            this.textBoxFolderComment.Text = this.folderCustomizer.Comment;
            this.memoEditDesctopIni.Text = this.folderCustomizer.IniText;
            this.setImage();
        }

        /// <summary>
        /// Sets the image.
        /// </summary>
        private void setImage()
        {
            this.pictureEditFolderImage.Tag = this.folderCustomizer.PathFolderImage;
            this.pictureEditFolderImage.Image = this.folderCustomizer.FolderImage;
        }

        /// <summary>
        /// Handles the TextChanged event of the textBoxFolderComment control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void textBoxFolderComment_TextChanged(object sender, EventArgs e)
        {
            folderCustomizer.Comment = this.textBoxFolderComment.Text;
        }

        /// <summary>
        /// Handles the EditValueChanged event of the pictureEdit1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void pictureEdit1_EditValueChanged(object sender, EventArgs e)
        {
            folderCustomizer.PathFolderImage = this.pictureEditFolderImage.Tag.ToString();
        }

        /// <summary>
        /// Handles the Click event of the ButtonApply control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonApply_Click(object sender, EventArgs e)
        {
            this.folderCustomizer.ApplyCustomization();
            this.loadFolderData();
        }

        /// <summary>
        /// Handles the Click event of the ButtonFolderCreate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonFolderCreate_Click(object sender, EventArgs e)
        {
            this.folderCustomizer.ApplyCustomization();
            this.loadFolderData();
        }

        /// <summary>
        /// Handles the Click event of the ButtonResetFolder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void buttonResetFolder_Click(object sender, EventArgs e)
        {
            this.folderCustomizer.ResetCustomization();
            this.loadFolderData();
        }

        /// <summary>
        /// Handles the TextChanged event of the textBoxFolderPath control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void textBoxFolderPath_TextChanged(object sender, EventArgs e)
        {
            this.folderCustomizer.Path = this.textBoxFolderPath.Text;
            this.loadFiles();
        }


        /// <summary>
        /// Handles the LinkClicked event of the linkLabelDeleteEmptyFolders control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void linkLabelDeleteEmptyFolders_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // gets toplevel subdirectories 
            string[] dirs = Directory.GetDirectories(this.folderCustomizer.Path, "*", SearchOption.AllDirectories);

            // gets all directories that are empty
            List<string> endFolders = new List<string>();
            foreach (string dir in dirs)
            {
                if (this.isDirectoryEmpty(dir))
                    endFolders.Add(dir);
            }

            // processes recursively all directories
            foreach (string dir in endFolders)
                this.clearRecursivelyDir(dir);
        }

        /// <summary>
        /// Clears the recursively dir.
        /// </summary>
        /// <param name="dir">The dir.</param>
        private void clearRecursivelyDir(string dir)
        {

            if (!Directory.Exists(dir))
                return;

            if (dir == this.folderCustomizer.Path)
                return;

            // check if the dir is empty (the folder doesn't have any files or folders)
            if (this.isDirectoryEmpty(dir))
                this.deleteDir(dir);

            string parentDir = Path.GetDirectoryName(dir);
            clearRecursivelyDir(parentDir);
        }

        /// <summary>
        /// Deletes the dir.
        /// </summary>
        /// <param name="dir">The dir.</param>
        /// <returns><c>true</c> if directory was deleted, <c>false</c> otherwise.</returns>
        private bool deleteDir(string dir)
        {
            try
            {
                Directory.Delete(dir, true);
            }
            catch { return false; }
            return !Directory.Exists(dir);
        }

        /// <summary>
        /// Determines whether the directory is empty
        /// </summary>
        /// <param name="dir">The dir.</param>
        /// <returns><c>true</c> if directory is empty; otherwise, <c>false</c>.</returns>
        private bool isDirectoryEmpty(string dir)
        {
            bool retVal = true;
            try
            {
                string[] dirs = Directory.GetDirectories(dir, "*", SearchOption.AllDirectories);
                retVal = dirs.Length == 0;

                // if there aren't any folders, check if there are some files
                if (retVal)
                {
                    string[] files = Directory.GetFiles(dir, "*", SearchOption.AllDirectories);
                    List<string> listFile = null;
                    if (files.Length != 0)
                        listFile = files.ToList();
                    if (listFile != null)
                    {
                        listFile.Sort();

                        if (listFile.Count == 1)
                        {
                            string file = Path.GetFileName(listFile[0]).ToLower();
                            if (file != "desktop.ini" && file != "thumbs.db")
                                retVal = false;
                        }
                        if (files.Length == 2)
                        {
                            string file1 = Path.GetFileName(listFile[0]).ToLower();
                            string file2 = Path.GetFileName(listFile[1]).ToLower();
                            if (file1 != "desktop.ini" || file2 != "thumbs.db")
                                retVal = false;
                        }
                        else if (files.Length > 1)
                            retVal = false;
                    }
                }
            }
            catch { retVal = false; }
            return retVal;
        }

        private bool enableLoadingFiles = false;


        private void tabbedControlGroupIcons_SelectedPageChanged(object sender, DevExpress.XtraLayout.LayoutTabPageChangedEventArgs e)
        {

            this.enableLoadingFiles = e.Page.Name == "layoutControlGroupFiles";
            this.loadFiles();

        }

        /// <summary>
        /// Loads the files.
        /// </summary>
        private void loadFiles()
        {
            this.fileList.Clear();
            if (!this.enableLoadingFiles) return;
            if (Directory.Exists(this.folderCustomizer.Path))
            {
                SearchOption option = this.checkEditSubFolder.Checked ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
                bool showDesktopIni = this.checkEditShowDesktopIni.Checked;

                string[] files = Directory.GetFiles(this.folderCustomizer.Path, "*", option);

                foreach (string file in files)
                {
                    MyFile myFile = new MyFile(file);
                    if (myFile.Name.ToLower() == "desktop.ini" && !showDesktopIni)
                        continue;
                    this.fileList.Add(myFile);
                }
                this.gridControl1.DataSource = this.fileList;
                this.gridControl1.RefreshDataSource();
                this.gridView1.ExpandAllGroups();
            }
        }

        /// <summary>
        /// Handles the CheckedChanged event of the checkEditSubFolder control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void checkEditSubFolder_CheckedChanged(object sender, EventArgs e)
        {
            this.loadFiles();
        }

        /// <summary>
        /// Handles the CheckedChanged event of the checkEditShowDesktopIni control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void dropDownButtonCopyList_Click(object sender, EventArgs e)
        {

            if (this.fileList.Count == 0)
                return;

            this.fileList.ModeList = this.barCheckItemOnlyFilesName.Checked ? MyFile.ModeListComposer.OnlyFileName : MyFile.ModeListComposer.FullPath;
            this.fileList.WithExtension = this.barCheckItemWithExtension.Checked;

            Clipboard.SetText(this.fileList.ToString());

        }

        /// <summary>
        /// Handles the Click event of the simpleButtonDeleteEmptySubFolders control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void simpleButtonDeleteEmptySubFolders_Click(object sender, EventArgs e)
        {
            // gets toplevel subdirectories 
            string[] dirs = Directory.GetDirectories(this.folderCustomizer.Path, "*", SearchOption.AllDirectories);

            // gets all directories that are empty
            List<string> endFolders = new List<string>();

            progressBarControl1.Properties.Maximum = dirs.Length;
            progressBarControl1.Position = 0;
            labelControl1.Text = "Analisi delle cartelle";
            progressBarControl1.Visible = true;
            labelControl1.Visible = true;
            Application.DoEvents();
           
            foreach (string dir in dirs)
            {
                try
                {
                    // Console.WriteLine(dir);
                    progressBarControl1.Position++;
                    Application.DoEvents();
                    if (this.isDirectoryEmpty(dir))
                        endFolders.Add(dir);
                }
                catch { }
            }

            progressBarControl1.Position = 0;
            progressBarControl1.Properties.Maximum = endFolders.Count;
            labelControl1.Text = "Eliminazione delle cartelle";
            Application.DoEvents();

            if (endFolders.Count != 0)
            {
                // processes recursively all directories
                foreach (string dir in endFolders)
                {
                    progressBarControl1.Position++;
                    Application.DoEvents();
                    this.clearRecursivelyDir(dir);
                }
            }

            progressBarControl1.Visible = false;
            labelControl1.Visible = false;
            Application.DoEvents();
        }

        /// <summary>
        /// Handles the CheckedChanged event of the checkEditShowDesktopIni control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void checkEditShowDesktopIni_CheckedChanged(object sender, EventArgs e)
        {
            this.loadFiles();
        }

        private void checkEditHidden_CheckedChanged(object sender, EventArgs e)
        {
            this.folderCustomizer.Hidden = checkEditHidden.Checked;
        }

        private void checkEditReadOnly_CheckedChanged(object sender, EventArgs e)
        {
            this.folderCustomizer.ReadOnly = checkEditReadOnly.Checked;
        }
    }
}
